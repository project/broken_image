(function ($) {
  Drupal.behaviors.broken_image = {
    attach: function (context, settings) {
     	
      /*$('img').bind("error", function() {
	  	  $(this).attr('src', Drupal.settings.broken_image.path);
	  	});*/

			$("img").each( function () {
			  var value = $(this);			 
				$.ajax({
				  url:$(this).attr('src'),
				  type:'get',
				  async: false,
				  error:function(response) {			    
				    var re_src = Drupal.settings.broken_image.path;
				    $.ajax({
				      url: re_src,
				      type:'get',
				      async: false,
				      success: function(){
				        $(value).attr('src', re_src);
				      },
				      error:function(response) {
				        $(value).hide();
				      }
				    });
				  }
				});
			});

    }
  };
}(jQuery));